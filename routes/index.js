var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Red Bicicletas', subtitle: 'La primera comunidad online de ciclistas urbanos' });
});

module.exports = router;