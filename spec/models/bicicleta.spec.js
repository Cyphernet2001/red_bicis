var Bicicleta = require('../../models/bicicleta-model');

beforeEach(() => {
  Bicicleta.allBicis = [];
});
describe('Bicicleta.allBicis', () => {
  it('Coleccion de bicicletas vacia', () => {
    expect(Bicicleta.allBicis.length).toBe(0);
  });
});

describe('Bicicleta.add', () => {
  it('agrega una bicicleta a la coleccion', () => {
    expect(Bicicleta.allBicis.length).toBe(0);

    var bicicleta = new Bicicleta(3, 'blanca', 'urbana', [54.1570000, -113.857000]);
    Bicicleta.add(bicicleta);

    expect(Bicicleta.allBicis[0]).toBe(bicicleta);
    expect(Bicicleta.allBicis.length).toBe(1);
  });
});

describe('Bicicleta.findById', () => {
  it('Busca Bicicleta por id', () => {
    expect(Bicicleta.allBicis.length).toBe(0);

    var bicicleta = new Bicicleta(2, 'blanca', 'urbana', [54.1570000, -113.857000]);
    Bicicleta.add(bicicleta);
    var targetBici = Bicicleta.findById(2);

    expect(targetBici.id).toBe(bicicleta.id);
    expect(targetBici.color).toBe(bicicleta.color);
    expect(targetBici.model).toBe(bicicleta.model);
  });
});

describe('Bicicleta.removeById', () => {
  it('Borra bicicleta por id', () => {
    expect(Bicicleta.allBicis.length).toBe(0);

    var bicicleta = new Bicicleta(1, 'blanca', 'urbana', [54.1570000, -113.857000]);
    Bicicleta.add(bicicleta);

    expect(Bicicleta.allBicis.length).toBe(1);

    Bicicleta.removeById(bicicleta.id);

    expect(Bicicleta.allBicis.length).toBe(0);
  });
});