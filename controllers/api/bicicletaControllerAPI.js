var Bicicleta = require('../../models/bicicleta-model');

exports.bicicleta_list = function(req, res) {
  res.status(200).json({
    bicicletas: Bicicleta.allBicis
  });
}

exports.bicicleta_create = function(req, res) {
  try {
    var bicicleta = new Bicicleta(req.body.id, req.body.color, req.body.model);
    bicicleta.location = [req.body.lat, req.body.long];
    Bicicleta.add(bicicleta);
    res.status(200).json({
      bicicleta: bicicleta
    });
  } catch (error) {
    res.status(404).send('Hubo un error en su solicitud.');
  }
};

exports.bicicleta_update = function(req, res) {
  try {
    var bicicleta = Bicicleta.findById(req.body.id);
    bicicleta.id = req.body.newid;
    bicicleta.color = req.body.color;
    bicicleta.model = req.body.model;
    bicicleta.location = [req.body.lat, req.body.long];
    res.status(200).json({
      mensaje: `La bicicleata ha sido actualizada con exito!`,
      bicicletaActualizada: bicicleta
    });
  } catch (error) {
    res.status(404).send('La bicicleta que desea actualizar no existe.');
  }
};

exports.bicicleta_delete = function(req, res) {
  try {
    var bicicleta = Bicicleta.findById(req.body.id);
    Bicicleta.removeById(bicicleta.id);
    res.status(204).send();
  } catch (error) {
    res.status(404).send('La bicicleta que desea borrar no existe.');
  }
};