var Bicicleta = function(id, color, model, location) {
  this.id = id;
  this.color = color;
  this.model = model;
  this.location = location;
}

Bicicleta.allBicis = [];

Bicicleta.prototype.toString = function() {
  return 'id: ' + this.id + ' | color: ' + this.color + ' | model: ' + this.model + ' | location: ' + this.location;
}

/**
 * Add a bicicleta into collection
 * @param {*} bicicleta 
 */
Bicicleta.add = function(bicicleta) {
  Bicicleta.allBicis.push(bicicleta);
}

Bicicleta.findById = function(bicicletaId) {
  var bicicleta = Bicicleta.allBicis.find(x => Number(x.id) === Number(bicicletaId));
  if (bicicleta) {
    return bicicleta;
  } else {
    throw new Error(`No existe una bicicleta con el id ${bicicletaId}`);
  }
}

Bicicleta.removeById = function(bicicletaId) {
  for (var i = 0; i < Bicicleta.allBicis.length; i++) {
    if (Bicicleta.allBicis[i].id === Number(bicicletaId)) {
      Bicicleta.allBicis.splice(i, 1);
      break;
    }
  }
}

/*
Bicicleta.add(new Bicicleta(1, 'roja', 'urbana', [54.1550000, -113.855000]));
Bicicleta.add(new Bicicleta(2, 'blanca', 'urbana', [54.1570000, -113.857000]));
*/
module.exports = Bicicleta;