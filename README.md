# Red de Bicicletas
*By [Martin Farias](/#)*

Aplicacion web, primer modulo del curso "Desarrollo del lado servidor: NodeJS, Express y MongoDB"

## Instalacion

``` bash
$ npm i
```

## Ejecucion entorno de produccion

``` bash
$ npm run start
```

## Ejecucion entorno de desarrollo

``` bash
$ npm run devstart
```

## Configuracion

Por defecto se servidor se ejecutara en el puerto 3000
