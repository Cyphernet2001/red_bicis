var mymap = L.map('main_map').setView([54.1550000, -113.855000], 15);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
  attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
}).addTo(mymap);

$.ajax({
  dataType: "json",
  url: "api/bicicletas",
  success: function(result) {
    result.bicicletas.forEach(function(bicicleta) {
      L.marker(bicicleta.location, { title: bicicleta.id }).addTo(mymap);
    });
  }
});